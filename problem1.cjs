const fs = require('fs')
const path = require('path')
const directoryPath = path.join(__dirname, './JsonDirectory')


/*
    Problem 1:
    
    Using callbacks and the fs module's asynchronous functions, do the following:
        1. Create a directory of random JSON files
        2. Delete those files simultaneously 
*/


function problem1() {

    return createDirectory()
        .then((dirpath)=>{
            return createjsonfiles(dirpath)
        })
        .then(deletefiles)

}

function createDirectory() {
    return new Promise((resolve, reject) => {
        fs.mkdir(directoryPath, { recursive: true }, (err) => {
            if (err) {
                reject(err)
            } else {
                console.log('SUCCESS: DIRECTORY CREATION')
                resolve(directoryPath)
            }
        })
    })
}

function createjsonfiles(dirpath) {
    return new Promise((resolve, reject) => {

        let randomNum = Math.ceil(Math.random() * 10)
        let filesList = new Array(randomNum).fill(0)
            .map((each, index) => {
                return `randomfile${each}${index}.json`
            })
        let promisesList = filesList
            .map((each, index) => {
                return createEachFile(dirpath, each, index)
            })

        Promise.all(promisesList)
            .then(() => {
                console.log("SUCCESS: ALL FILES CREATED")
                resolve(filesList)
            })
            .catch(() => {
                reject(err)
            })
    })

}

function createEachFile(dirpath, each, index) {
    return new Promise((resolve, reject) => {
        fs.writeFile(path.join(dirpath, each), JSON.stringify(`${index}randomjsondata`), (err) => {
            if (err) {
                console.err(`FAILED: To Write ${each}`)
                reject(err)
            } else {
                console.log(`SUCCESS: Created ${each}`)
                resolve()
            }
        })

    })
}

function deleteEachFile(each) {
    return new Promise((resolve, reject) => {
        fs.unlink(path.join(directoryPath, each), (err) => {
            if (err) {
                console.error(`ERROR: DELETION ${each}`)
                reject(err)
            } else {
                console.log(`SUCCESS: ${each} DELETION`)
                resolve()
            }
        })

    })
}

function deletefiles(filesList) {
    return new Promise((resolve, reject) => {

        let deletionPromises = filesList
            .map(each => {
                return deleteEachFile(each)
            })

        Promise.all(deletionPromises)
            .then(() => {
                console.log(`SUCCESS: ALL FILES DELETED`)
                resolve()
            })
            .catch((err) => {
                reject(err)
            })
    })
}

module.exports = problem1
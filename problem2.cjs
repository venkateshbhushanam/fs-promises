const fs = require('fs')
const path = require('path')

const readfilePath = path.join(__dirname, './lipsum.txt')
const uppercaseFilePath = path.join(__dirname, './uppercaseFile')
const fileNamestxtPath = path.join(__dirname, './filenames.txt')
const lowercaseSentanceFilePath = path.join(__dirname, './lowercaseSentanceFile')
const sortedFilePath = path.join(__dirname, './sortedFile')

/*
    Problem 2:
    
    Using callbacks and the fs module's asynchronous functions, do the following:
        1. Read the given file lipsum.txt
        2. Convert the content to uppercase & write to a new file. Store the name of the new file in filenames.txt
        3. Read the new file and convert it to lower case. Then split the contents into sentences. Then write it to a new file. Store the name of the new file in filenames.txt
        4. Read the new files, sort the content, write it out to a new file. Store the name of the new file in filenames.txt
        5. Read the contents of filenames.txt and delete all the new files that are mentioned in that list simultaneously.
*/

function problem2() {

    return readfile()
        .then((UpperCaselipsumData) => {
            return storedataInNewFile(UpperCaselipsumData, uppercaseFilePath)
        })
        .then((upperCasepath) => {
            return writeFileNameInfileNamestxt(upperCasepath)
        })
        .then(()=>{
            return readUpperCaseContentToSentances()
        })
        .then((sentacedata) => {
            return storedataInNewFile(sentacedata, lowercaseSentanceFilePath)
        })
        .then((sentacedataFilepath) => {
            return writeFileNameInfileNamestxt(sentacedataFilepath)
        })
        .then((sentacedataPath)=>{
            return sortSentacedData(sentacedataPath)
        })
        .then((sortedData) => {
            return storedataInNewFile(sortedData, sortedFilePath)
        })
        .then((sortedFilePath) => {
            return writeFileNameInfileNamestxt(sortedFilePath)

        })
        .then(() => {
            return deleteAllFilesInFileNametxt(fileNamestxtPath)
        })
        .then(() => {
            delteFileNamestxt()
        })

}

function delteFileNamestxt() {
    return new Promise((resolve, reject) => {
        fs.unlink(fileNamestxtPath, (err) => {
            if (err) {
                console.error(err)
                reject()
            } else {
                console.log(`SUCCESS: filenames.txt Deleted`)
                resolve()
            }
        })
    })
}

function deleteEachFile(eachFileName) {
    return new Promise((resolve, reject) => {
        fs.unlink(path.join(__dirname, eachFileName), (err) => {
            if (err) {
                console.error(`FAILED: To Delete ${eachFileName}`)
                reject(err)
            } else {
                console.log(`SUCCESS: ${eachFileName} Deleted`)
                resolve()
            }
        })
    })
}

function deleteAllFilesInFileNametxt(fileNamestxtPath) {
    return new Promise((resolve, reject) => {
        fs.readFile(fileNamestxtPath, (err, fileNames) => {
            if (err) {
                console.err(`FAILED: Reading ${fileNamestxtPath}`)
                reject(err)
            } else {

                let fileNamesList = fileNames.toString().split('./')

                    .filter(eachFileName => {
                        if (eachFileName) {
                            return true
                        } else {
                            return false
                        }
                    })

                let deletePromises = fileNamesList
                    .map(eachFileName => {
                        return deleteEachFile(eachFileName)
                    })
                Promise.all(deletePromises)
                    .then(() => {
                        console.log(`All Files Deleted`)
                        resolve()
                    })
                    .catch((err) => {
                        console.error(err)

                    })
            }
        })
    })
}

function sortSentacedData(sentanceDataFilePath) {
    return new Promise((resolve, reject) => {
        fs.readFile(sentanceDataFilePath, (err, sentacedata) => {
            if (err) {
                console.err(`FAILED: To Read ${sentanceDataFilePath}`)
                reject(err)
            } else {

                let sentanceArray = sentacedata.toString().split("\n")
                let sortedData = sentanceArray

                    .map(eachLine => {
                        return eachLine.trim()
                    })
                    .filter(eachLine => {
                        return eachLine

                    }).sort()

                console.log(`SUCCESS: Data Sorted`)
                resolve(sortedData.join('\n'))

            }
        })
    })
}

function readUpperCaseContentToSentances() {
    return new Promise((resolve, reject) => {
        fs.readFile(uppercaseFilePath, (err, data) => {
            if (err) {
                console.err(`FAILED: Reading UpperCaseFile`)
                reject(err)

            } else {
                console.log(`SUCCESS: Reading UpperCaseFile`)
                let lowerCaseData = data.toString().toLowerCase()
                let sentences = lowerCaseData.split(".").join('\n')
                resolve(sentences)
            }
        })
    })

}

function writeFileNameInfileNamestxt(currentFilePath) {
    return new Promise((resolve, reject) => {

        let fileName = "./" + currentFilePath.split('/').pop()

        fs.appendFile(fileNamestxtPath, fileName, (err) => {
            if (err) {
                console.error("FAILED: WRITING")
                reject(err)
            } else {
                console.log(`SUCCESS: ${fileName} name store in filenames.txt`)
                resolve(currentFilePath)
            }
        })
    })
}

function storedataInNewFile(data, filePathtoStore) {
    //filePathtoStore contains the new file path
    return new Promise((resolve, reject) => {
        fs.writeFile(filePathtoStore, data, (err) => {
            if (err) {
                console.log('FAILED: To Write')
                reject(err)

            } else {
                console.log("SUCCESS: Writing Data In New File")
                resolve(filePathtoStore)

            }
        })
    })

}

function readfile() {
    return new Promise((resolve, reject) => {
        fs.readFile(readfilePath, (err, data) => {
            if (err) {
                console.log('FAILED: File Read Operation')
                console.err(err)
                reject(err)
            } else {
                console.log("SUCCESS: Reading Input File")
                resolve(data.toString().toUpperCase())
            }
        })
    })
}


module.exports = problem2